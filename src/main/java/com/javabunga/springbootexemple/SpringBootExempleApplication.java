package com.javabunga.springbootexemple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootExempleApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootExempleApplication.class, args);
//        System.out.println("Hello world");
    }

}
